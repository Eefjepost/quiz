package com.example.quiz;

import com.example.quiz.Repository.AnswerOptionsRepository;
import com.example.quiz.Repository.QuestionsRepository;
import com.example.quiz.model.Answeroptions;
import com.example.quiz.model.Questions;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class QuizApplication {


    public static void main(String[] args) {

        SpringApplication.run(QuizApplication.class);
    }
       @Bean
        public CommandLineRunner demo(QuestionsRepository repository, AnswerOptionsRepository answerOptionsRepository) {
            return (args) -> {

                Questions questions = new Questions("Question one");
                // save a few customers
                repository.save(questions);


                //save answeroptions
                answerOptionsRepository.save(new Answeroptions(1, "answer A", true, questions ));
                answerOptionsRepository.save(new Answeroptions(2, "answer B", false, questions ));
                answerOptionsRepository.save(new Answeroptions(3, "answer C",false, questions ));
            };


        }




    }



