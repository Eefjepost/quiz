package com.example.quiz.Repository;

import com.example.quiz.model.Answeroptions;
import com.example.quiz.model.Questions;
import com.example.quiz.model.SurveyAnswers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.Optional;

@Repository
public interface SurveyAnswersRepository extends JpaRepository<SurveyAnswers, Long> {

 //   Optional<SurveyAnswers> findById(Long id);

// Optional<SurveyAnswers> findChoosenAnswerByQuestionId(Long questionid);

}


