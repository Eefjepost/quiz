package com.example.quiz.Repository;
import java.util.Optional;
import com.example.quiz.model.Questions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionsRepository extends JpaRepository<Questions, Long> {


// deze methode kan een vraagomschrijving 'getten' bij het invoegen/meegeven van een ID
Optional<Questions> findById(Long id);


}