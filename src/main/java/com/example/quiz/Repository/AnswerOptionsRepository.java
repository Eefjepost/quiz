package com.example.quiz.Repository;

import com.example.quiz.model.Answeroptions;
import com.example.quiz.model.Questions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
    public interface AnswerOptionsRepository extends JpaRepository<Answeroptions, Long> {
        //List<AnswerExplanations> findAnswerOptionsByAnswerexid(Long answerexid);


  //  List<Answeroptions> findByQuestions(Questions questions, Sort sort);
    //Vindt de antwoordmogelijkheden door het id in te geven van de bijbehorende tabel (dus answeroptions id).
    Optional<Answeroptions> findById(Long id);

    //Vindt de antwoordmogelijkheden door de vraag id mee op te geven die erbij hoort.
    List<Answeroptions> findByQuestionsId(Long questionid);

    }

