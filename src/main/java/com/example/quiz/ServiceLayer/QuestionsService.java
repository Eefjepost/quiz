package com.example.quiz.ServiceLayer;

import com.example.quiz.Repository.QuestionsRepository;
import com.example.quiz.model.Questions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionsService {

    @Autowired
    private QuestionsRepository repository;

    public List<Questions> findAll() {
        List<Questions> questions = (List<Questions>) repository.findAll();
        return questions;
    }

    public Optional<Questions> findQuestionById(Long id){
        return repository.findById(id);

    }


    }


