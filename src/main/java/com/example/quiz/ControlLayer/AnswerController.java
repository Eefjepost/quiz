package com.example.quiz.ControlLayer;

import com.example.quiz.Repository.AnswerOptionsRepository;
import com.example.quiz.Repository.SurveyAnswersRepository;
import com.example.quiz.Repository.QuestionsRepository;
import com.example.quiz.model.Answeroptions;
import com.example.quiz.model.SurveyAnswers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AnswerController {

@Autowired
private AnswerOptionsRepository answerOptionsRepository;


@Autowired
private QuestionsRepository questionsRepository;

//bij het ingeven van een questionid, neemt deze de antwoordopties mee terug.
    @GetMapping("/questions/{questionid}/answeroptions")
    public List<Answeroptions> getAllAnsweroptionsByQuestionsId(@PathVariable(value = "questionid") Long questionid){
        return answerOptionsRepository.findByQuestionsId(questionid);
    }




}

