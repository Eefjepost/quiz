package com.example.quiz.ControlLayer;
import com.example.quiz.Repository.AnswerOptionsRepository;
import com.example.quiz.Repository.QuestionsRepository;
import com.example.quiz.ServiceLayer.QuestionsService;
import com.example.quiz.model.Answeroptions;
import com.example.quiz.model.Questions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
public class QuestionsController {

    @Autowired
    private QuestionsService questionsService;
    private final QuestionsRepository questionsRepository;


    @Autowired
    public QuestionsController(QuestionsRepository questionsRepository) {
        this.questionsRepository = questionsRepository;
    }

    // wat de findall methode precies doet staat beschreven in de serviceclass van Questions.
    @GetMapping(value = "/questions")
    public List findAll() {
        return questionsService.findAll();
    }


    @GetMapping(value = "/questionbyid/{id}")
    public Questions findQuestionByid(@PathVariable("id") Long id) {
        return questionsService.findQuestionById(id).orElseThrow(() -> new IllegalArgumentException("Not found"));
    }




}

//haal een question (en alle bijbehorende kolommen zoals in de service class gedefinieerd) uit de database door een ID in te geven.
  /*  @GetMapping("/questions/{id}")
    public Questions getQuestionById(@PathVariable("id") Long id) throws NotFoundException {
        return questionsRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("question %d not found", id)));
    }

    //get answeroptions with questionid
    @GetMapping("/questions/{questionId}/answerOptions")
    public List<AnswerExplanations> getAnswersOptionsByAnswerexId(@PathVariable Long answerexid) {
        return answerExplanationsRepository.findAnswerOptionsByAnswerexid(answerexid);
    }

 /*   @PostMapping("/questions")
    public Questions createQuestions(@RequestBody Questions questions) {
        return questionsRepository.save(questions);
    }
    */




 //   @GetMapping
  //  public String findQuestions(Model model) {


      //  List<Questions> questions = (List<Questions>) questionsService.findAll();

      //  model.addAttribute("questions", questions);

      //  return "showQuestions";
   // }







    /*   @PostMapping("/questions")
       public ResponseEntity<Object> createQuestion(@RequestBody Questions question) {
           Questions savedQuestion = questionsRepository.save(question);

           URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                   .buildAndExpand(savedQuestion.getQuestionid()).toUri();

           return ResponseEntity.created(location).build();

       }


       @GetMapping("/questions/{id}")
       public List <Questions> retrieveQuestion(@PathVariable long id) {
           return questionsRepository.findById(id);
       }
   */

//het toevoegen van een question
/*  @PostMapping
    public Questions addQuestion(@RequestBody Questions question) {
      return questionsRepository.save(question);
    }
    */







