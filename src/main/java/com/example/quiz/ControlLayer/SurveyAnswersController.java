package com.example.quiz.ControlLayer;

import com.example.quiz.Repository.QuestionsRepository;
import com.example.quiz.Repository.SurveyAnswersRepository;
import com.example.quiz.model.Questions;
import com.example.quiz.model.SurveyAnswers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class SurveyAnswersController {


    @Autowired
    private SurveyAnswersRepository surveyAnswersRepository;


    @Autowired
    private QuestionsRepository questionsRepository;


    //het ingeven van een antwoord (choosenAnswer) naar de database
    @PostMapping("/choosenanswer")
    public SurveyAnswers addAnswer(@RequestBody SurveyAnswers surveyanswer) {
      surveyanswer.addQuestions(surveyanswer.getQuestion());
        return surveyAnswersRepository.save(surveyanswer);

    }

}

 /*   //bij het ingeven van een questionid, geeft deze het antwoord dat gekozen is door de user mee terug
    @GetMapping("/questions/{questionid}/choosenanswer")
    public Optional<SurveyAnswers> getChoosenAnswerByQuestionsId(@PathVariable(value = "questionid") Long questionid){
        return surveyAnswersRepository.findChoosenAnswerByQuestionId(questionid);
    }

  */

