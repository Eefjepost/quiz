package com.example.quiz.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class SurveyAnswers {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;
   private String choosenAnswer;
   private int userId;

 @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade=CascadeType.ALL)
  @JoinColumn(name = "questionid", nullable = false, updatable = false)
 private Questions question = new Questions();


    protected SurveyAnswers() {
    }


    public SurveyAnswers(int userId, String choosenAnswer, Questions question) {
        this.userId = userId;
        this.choosenAnswer= choosenAnswer;
     this.question = question;

    }


    public String surveyAnswersToString() {
        return String.format(
                "SurveyAnswers[id=%d,userId=%d, choosenAnswer='%s']",
                id, userId, choosenAnswer);
    }

    public Questions getQuestion()
    {
        return question;
    }

    public Questions addQuestions(Questions question){
        this.question = question;
        return question;
    }

    public Questions getQuestion(Questions question){
        return question;
    }

    public Long getQuestionid() {
        return question.getid();
    }

    public String setChoosenAnswer() {
        return choosenAnswer;
    }

    public String getChoosenAnswer() {
        return choosenAnswer;
    }

    public Long getId() {
        return id;
    }

    public int setUserId() {
        return userId;
    }

    public int getUserId() {
        return userId;
    }



}
