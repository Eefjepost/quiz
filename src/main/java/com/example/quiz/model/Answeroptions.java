package com.example.quiz.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

import javax.persistence.*;

@Entity
public class Answeroptions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int number;
private String answerOptions;
private Boolean isCorrect;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "questionid", nullable = false)
    private Questions questions;


    protected Answeroptions() {

    }

    public Answeroptions(int number, String answerOptions, Boolean isCorrect, Questions questions) {
        this.number= number;
        this.answerOptions = answerOptions;
        this.isCorrect = isCorrect;
        this.questions = questions;
    }

    public String getanswerOptions() {
        return answerOptions;
    }

    public Boolean getIsCorrect() {
        return isCorrect;
    }


    public String AnsweroptionsToString() {
        return String.format(
                "AnswerOptions[number=%d, answerOptions='%s', isCorrect='%s' ]",
                number, answerOptions, isCorrect);
    }





}



