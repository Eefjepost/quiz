package com.example.quiz.model;

import javax.persistence.*;
import java.util.Set;


@Entity
public class Questions{
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="questionid", unique = true,
                updatable = false,
                nullable = false)
        private Long id;
        private String questionDescription;
        @OneToMany(mappedBy = "questions", fetch = FetchType.LAZY, cascade = CascadeType.ALL )
        private Set<Answeroptions> answerOptions;


     //   @OneToMany(fetch = FetchType.EAGER)
      //  @JoinColumn(columnDefinition = "answerexid")
   //     private List<AnswerExplanations> answerExplanationsList;


        public Questions() {}


        public Questions(String questionDescription) {

                this.questionDescription = questionDescription;

        }



        public String questionToString() {
                return String.format(
                        "Question[id=%d, questiondescription='%s']",
                        id, questionDescription, answerOptions);
        }


        public Long getid() {
                return id;
        }

        public String getQuestionDescription() {
                return questionDescription;
        }

        public Set<Answeroptions> getAnswerOptions() { return answerOptions; }


}