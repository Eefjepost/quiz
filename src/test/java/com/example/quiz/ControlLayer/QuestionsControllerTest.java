package com.example.quiz.ControlLayer;

import com.example.quiz.QuizApplication;
import com.example.quiz.Repository.QuestionsRepository;
import com.example.quiz.ServiceLayer.QuestionsService;
import com.example.quiz.model.Questions;

import org.assertj.core.api.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.jupiter.api.Assertions;

import static org.junit.Assert.*;
import org.junit.jupiter.api.extension.ExtendWith;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.ContentResultMatchers.*;
import  static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.runner.RunWith.*;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class QuestionsControllerTest {

    @InjectMocks
    private QuestionsController questionsController;

    @Mock
    QuizApplication quizapplication;

    private MockMvc mockMvc;

    private QuestionsRepository questionsRepository;
    private QuestionsService questionservice;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(this.questionsController).build();
    }

 //   @Test
  //  public void testFindQuestionByid() throws Exception {
        // build your expected results here

   //     MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("http://localhost:8080//questionbyid/1")).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();


     //   assertEquals((Object) "response does not match", mvcResult.getResponse().getContentAsString(), );

        // verify the calls
    //}
    }


